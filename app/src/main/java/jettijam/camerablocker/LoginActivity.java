package jettijam.camerablocker;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {
    EditText mPhoneEditText;
    EditText mNameEditText;
    Button mLoginButton;
    Button mStopButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPhoneEditText = (EditText) findViewById(R.id.login_phone_edittext);
        mNameEditText = (EditText) findViewById(R.id.login_name_edittext);
        mLoginButton = (Button) findViewById(R.id.login_login_button);
        mStopButton = (Button)findViewById(R.id.login_stop_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = mPhoneEditText.getText().toString();
                String name = mNameEditText.getText().toString();

                new LoginTask(number, name).execute();
            }
        });
        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent stopIntent = new Intent(LoginActivity.this, CameraBlockService.class);
                stopService(stopIntent);
            }
        });
    }

    class LoginTask extends AsyncTask<Void, Void, Boolean>{
        String mNumber;
        String mName;

        public LoginTask(String number, String name){
            this.mNumber = number;
            this.mName = name;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
    }
}
