package jettijam.camerablocker;

import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
//    Camera mCamera;
//    int mCameraId;
//    Button mStopButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent();
        intent.setClass(this, CameraBlockService.class);
        startService(intent);



//        if (!getPackageManager()
//                .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
//            Toast.makeText(this, "There is no camera", Toast.LENGTH_SHORT).show();
//
//            return;
//        } else {
//            mCameraId = findFrontFacingCamera();
//
//            if (mCameraId < 0) {
//                Toast.makeText(this, "There is no camera",
//                        Toast.LENGTH_LONG).show();
//                return;
//            } else {
//                mCamera = Camera.open(mCameraId);
//
//            }
//        }


    }

    private int findFrontFacingCamera() {
        int id = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {

                id = i;
                break;
            }
        }
        return id;
    }
}
