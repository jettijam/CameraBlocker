package jettijam.camerablocker;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.hardware.Camera;
import android.os.IBinder;

public class CameraBlockService extends Service {

    Camera mCamera;

    public CameraBlockService() {
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(android.R.drawable.ic_input_delete)
                .setContentTitle("Camera Blocker")
                .setTicker("Blocking Camera ..")
                .setWhen(System.currentTimeMillis());

        Notification notification = builder.build();

        startForeground(1, notification);


        mCamera = Camera.open();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public boolean stopService(Intent name) {
        mCamera.release();
        return super.stopService(name);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.


        throw new UnsupportedOperationException("Not yet implemented");
    }
}
